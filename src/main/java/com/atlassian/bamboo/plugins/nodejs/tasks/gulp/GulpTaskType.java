package com.atlassian.bamboo.plugins.nodejs.tasks.gulp;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.util.concurrent.NotNull;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class GulpTaskType implements CommonTaskType
{
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;
    private final I18nResolver i18nResolver;

    // ---------------------------------------------------------------------------------------------------- Constructors
    public GulpTaskType(final ProcessService processService, final EnvironmentVariableAccessor environmentVariableAccessor, CapabilityContext capabilityContext, I18nResolver i18nResolver)
    {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
        this.i18nResolver = i18nResolver;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public TaskResult execute(@NotNull CommonTaskContext taskContext) throws TaskException
    {
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext);

        try
        {
            final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

            final String nodeRuntime = configurationMap.get(GulpConfigurator.NODE_RUNTIME);
            final String nodePath = capabilityContext.getCapabilityValue(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_PREFIX + "." + nodeRuntime);
            Preconditions.checkState(StringUtils.isNotBlank(nodePath), i18nResolver.getText("node.runtime.error.undefinedPath"));

            final String gulpPath = configurationMap.get(GulpConfigurator.GULP_RUNTIME);
            Preconditions.checkState(StringUtils.isNotBlank(gulpPath), i18nResolver.getText("gulp.runtime.error.undefinedPath"));

            final String task = configurationMap.get(GulpConfigurator.TASK);
            final String configFile = configurationMap.get(GulpConfigurator.CONFIG_FILE);

            final Map<String, String> extraEnvironmentVariables = environmentVariableAccessor.splitEnvironmentAssignments(configurationMap.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), false);

            final ImmutableList.Builder<String> commandListBuilder = ImmutableList.<String>builder().add(nodePath, gulpPath);
            if (StringUtils.isNotBlank(configFile))
            {
                commandListBuilder.add("--gulpfile", configFile);
            }

            commandListBuilder.addAll(CommandlineStringUtils.tokeniseCommandline(task));

            taskResultBuilder.checkReturnCode(processService.executeExternalProcess(taskContext, new ExternalProcessBuilder()
                    .command(commandListBuilder.build())
                    .env(extraEnvironmentVariables)
                    .workingDirectory(taskContext.getWorkingDirectory())));

            return taskResultBuilder.build();
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
    }
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
