package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.google.common.base.Joiner;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

/**
 * A helper class for initializing and deleting the temporary cache directory for {@link NpmTaskType}.
 * <p/>
 * Cache directories are stored per agent and exist from initialization (first {@link NpmTaskType} execution within a
 * job or deployment) until either the build completes (and {@link NpmCacheCleanupProcessor} cleans it up) or another
 * job or deployment is executed on the same agent with {@link NpmTaskType} (then the cache directory is cleaned).
 */
public class NpmIsolatedCacheHelper
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(NpmIsolatedCacheHelper.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String CACHE_DIR_PATH = Joiner.on(File.separator).join("bamboo-nodejs-plugin", "npm-cache-%d");

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    private NpmIsolatedCacheHelper()
    {
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods

    /**
     * Initializes the temporary npm cache directory for the given agent. If the directory doesn't exist, it's created;
     * otherwise its content is deleted.
     *
     * @param agentId id of the agent
     *
     * @return a {@link File} pointing to an existing, empty cache directory
     *
     * @throws IOException
     */
    @NotNull
    static File initializeTemporaryCacheDirectory(long agentId) throws IOException
    {
        final File cacheDirectory = getCacheDirectory(agentId);

        // a highly unlikely scenario
        if (cacheDirectory.exists() && !cacheDirectory.isDirectory())
        {
            log.info(String.format("Removing a file that has the the same name as cache folder but isn't a directory (agent id %d): %s", agentId, cacheDirectory.getAbsolutePath()));
            FileUtils.forceDelete(cacheDirectory);
        }

        if (!cacheDirectory.exists())
        {
            log.debug(String.format("Creating new cache directory (agent id %d): %s", agentId, cacheDirectory.getAbsolutePath()));
            FileUtils.forceMkdir(cacheDirectory);
        }
        else
        {
            log.debug(String.format("Cleaning content of cache directory (agent id %d): %s", agentId, cacheDirectory.getAbsolutePath()));
            FileUtils.cleanDirectory(cacheDirectory);
        }
        return cacheDirectory;
    }

    /**
     * Deletes the temporary npm cache directory for the given agent. Does nothing if the directory doesn't exist.
     *
     * @param agentId id of the agent
     *
     * @throws IOException
     */
    static void deleteTemporaryCacheDirectory(long agentId) throws IOException
    {
        final File cacheDirectory = getCacheDirectory(agentId);
        if (cacheDirectory.exists())
        {
            log.debug(String.format("Deleting cache directory (agent id %d): %s", agentId, cacheDirectory.getAbsolutePath()));
            FileUtils.deleteDirectory(cacheDirectory);
        }
        else
        {
            // only debug level logging - if nothing gets cleaned then it probably wasn't meant to
            log.debug(String.format("Called to delete npm cache directory but it didn't exist (agent id %d): %s", agentId, cacheDirectory.getAbsolutePath()));
        }
    }

    // -------------------------------------------------------------------------------------------------- Helper Methods

    /**
     * Returns the {@link File} representing the npm cache directory for the specified agent.
     * <p/>
     * The file might not exist.
     *
     * @param agentId id of the agent
     *
     * @return cache directory
     */
    @NotNull
    private static File getCacheDirectory(long agentId)
    {
        return new File(FileUtils.getTempDirectory(), String.format(CACHE_DIR_PATH, agentId));
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}