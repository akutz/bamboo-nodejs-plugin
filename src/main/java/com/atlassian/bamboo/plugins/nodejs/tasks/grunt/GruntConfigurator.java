package com.atlassian.bamboo.plugins.nodejs.tasks.grunt;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Map;
import java.util.Set;

public class GruntConfigurator extends AbstractNodeRequiringTaskConfigurator
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String GRUNT_DEFAULT_EXECUTABLE = Joiner.on(File.separator).join("node_modules", "grunt-cli", "bin", "grunt");

    public static final String GRUNT_RUNTIME = "gruntRuntime";
    public static final String TASK = "task";
    public static final String CONFIG_FILE = "configFile";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .addAll(AbstractNodeRequiringTaskConfigurator.FIELDS_TO_COPY)
            .add(GRUNT_RUNTIME)
            .add(TASK)
            .add(CONFIG_FILE)
            .build();
    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .putAll(AbstractNodeRequiringTaskConfigurator.DEFAULT_FIELD_VALUES)
            .put(GRUNT_RUNTIME, GRUNT_DEFAULT_EXECUTABLE)
            .build();

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);
        if (StringUtils.isBlank(params.getString(GRUNT_RUNTIME)))
        {
            errorCollection.addError(GRUNT_RUNTIME, i18nResolver.getText("grunt.runtime.error.empty"));
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    @NotNull
    @Override
    public Set<String> getFieldsToCopy()
    {
        return FIELDS_TO_COPY;
    }

    @NotNull
    @Override
    public Map<String, Object> getDefaultFieldValues()
    {
        return DEFAULT_FIELD_VALUES;
    }
}
