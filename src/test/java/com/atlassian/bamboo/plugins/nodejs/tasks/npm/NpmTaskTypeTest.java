package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper.NPM_EXECUTABLE_NAME;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class NpmTaskTypeTest
{
    @Test
    @Parameters
    public void testConvertNodePathToNpmPath(@NotNull final String nodePath, @NotNull final String expectedNpmPath)
    {
        assertThat("Should properly convert Node.js executable path to npm executable path",
                NpmTaskType.convertNodePathToNpmPath(nodePath), equalTo(expectedNpmPath));
    }

    private Object[] parametersForTestConvertNodePathToNpmPath()
    {
        return new Object[]{
                new Object[]{"/usr/bin/node", "/usr/bin/" + NPM_EXECUTABLE_NAME},
                new Object[]{"/usr/bin/nodejs", "/usr/bin/" + NPM_EXECUTABLE_NAME},
                new Object[]{"/opt/node-v0.10/bin/node", "/opt/node-v0.10/bin/" + NPM_EXECUTABLE_NAME},
                new Object[]{"/opt/node-v0.10/bin/node.js", "/opt/node-v0.10/bin/" + NPM_EXECUTABLE_NAME},
                new Object[]{"~/node", "~/" + NPM_EXECUTABLE_NAME},
                new Object[]{"node", NPM_EXECUTABLE_NAME},
                new Object[]{"C:\\Program Files\\Node.js\\node.exe", "C:\\Program Files\\Node.js\\" + NPM_EXECUTABLE_NAME}
        };
    }
}