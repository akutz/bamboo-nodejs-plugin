package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.gulp.GulpConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;
import org.apache.log4j.Logger;

import java.util.Map;

public class GulpTaskComponent extends ComponentWithAdvancedOptions implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(GulpTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Gulp";

    // ------------------------------------------------------------------------------------------------- Type Properties
    @ElementBy(name = GulpConfigurator.NODE_RUNTIME)
    private SelectElement nodeRuntimeField;

    @ElementBy(name = GulpConfigurator.GULP_RUNTIME)
    private TextElement gulpRuntimeField;

    @ElementBy(name = GulpConfigurator.TASK)
    private TextElement taskField;

    @ElementBy(name = GulpConfigurator.CONFIG_FILE)
    private TextElement configFileField;

    @ElementBy(name = TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        this.withAdvancedOptions();

        if (config.containsKey(GulpConfigurator.NODE_RUNTIME))
        {
            nodeRuntimeField.select(Options.value(config.get(GulpConfigurator.NODE_RUNTIME)));
        }
        if (config.containsKey(GulpConfigurator.GULP_RUNTIME))
        {
            gulpRuntimeField.setText(config.get(GulpConfigurator.GULP_RUNTIME));
        }
        if (config.containsKey(GulpConfigurator.TASK))
        {
            taskField.setText(config.get(GulpConfigurator.TASK));
        }
        if (config.containsKey(GulpConfigurator.CONFIG_FILE))
        {
            configFileField.setText(config.get(GulpConfigurator.CONFIG_FILE));
        }
        if (config.containsKey(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES))
        {
            environmentVariablesField.setText(config.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES));
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}