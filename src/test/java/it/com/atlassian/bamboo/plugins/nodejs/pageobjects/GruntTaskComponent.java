package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.grunt.GruntConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;
import org.apache.log4j.Logger;

import java.util.Map;

public class GruntTaskComponent extends ComponentWithAdvancedOptions implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(GruntTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Grunt 0.4.x";

    // ------------------------------------------------------------------------------------------------- Type Properties
    @ElementBy(name = GruntConfigurator.NODE_RUNTIME)
    private SelectElement nodeRuntimeField;

    @ElementBy(name = GruntConfigurator.GRUNT_RUNTIME)
    private TextElement gruntRuntimeField;

    @ElementBy(name = GruntConfigurator.TASK)
    private TextElement taskField;

    @ElementBy(name = GruntConfigurator.CONFIG_FILE)
    private TextElement configFileField;

    @ElementBy(name = TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        this.withAdvancedOptions();

        if (config.containsKey(GruntConfigurator.NODE_RUNTIME))
        {
            nodeRuntimeField.select(Options.value(config.get(GruntConfigurator.NODE_RUNTIME)));
        }
        if (config.containsKey(GruntConfigurator.GRUNT_RUNTIME))
        {
            gruntRuntimeField.setText(config.get(GruntConfigurator.GRUNT_RUNTIME));
        }
        if (config.containsKey(GruntConfigurator.TASK))
        {
            taskField.setText(config.get(GruntConfigurator.TASK));
        }
        if (config.containsKey(GruntConfigurator.CONFIG_FILE))
        {
            configFileField.setText(config.get(GruntConfigurator.CONFIG_FILE));
        }
        if (config.containsKey(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES))
        {
            environmentVariablesField.setText(config.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES));
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}