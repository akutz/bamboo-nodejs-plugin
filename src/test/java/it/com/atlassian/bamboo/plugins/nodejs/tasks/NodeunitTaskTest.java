package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.tasks.JUnitParserTaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.nodeunit.NodeunitConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NodeunitTaskComponent;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NodeunitTaskTest extends AbstractNodeTaskTest
{
    @Test
    public void testTaskWithParsingEnabled() throws Exception
    {
        final Map<String, String> nodeunitTaskConfig = ImmutableMap.of(
                NodeunitConfigurator.TEST_FILES, "test/nodeunit/",
                NodeunitConfigurator.TEST_RESULTS_DIRECTORY, "test-reports/",
                NodeunitConfigurator.PARSE_TEST_RESULTS, Boolean.TRUE.toString());

        final TestBuildDetails plan = createNodeunitPlan(nodeunitTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);

        assertThat("Should have found 1 successful test", getNumberOfSuccessfulTestsForBuild(plan.getKey(), 1), is(1));
    }

    @Test
    public void testTaskWithJUnitParser() throws Exception
    {
        final Map<String, String> nodeunitTaskConfig = ImmutableMap.of(
                NodeunitConfigurator.TEST_FILES, "test/nodeunit/",
                NodeunitConfigurator.TEST_RESULTS_DIRECTORY, "test-reports/",
                NodeunitConfigurator.PARSE_TEST_RESULTS, Boolean.FALSE.toString());

        final TestBuildDetails plan = createNodeunitPlan(nodeunitTaskConfig);

        // assert that with parsing disabled we don't see any test results
        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);

        assertThat("Shouldn't have found any test results", getTotalNumberOfTestsForBuild(plan.getKey(), 1), is(0));

        // add JUnit parser task
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());
        final Map<String, String> jUnitTaskConfig = ImmutableMap.of(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN, "test-reports/");
        taskConfigurationPage.addNewTask(JUnitParserTaskComponent.getName(), JUnitParserTaskComponent.class, null, jUnitTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 2);

        assertThat("Should have found 1 successful test", getNumberOfSuccessfulTestsForBuild(plan.getKey(), 2), is(1));
    }

    private TestBuildDetails createNodeunitPlan(@NotNull Map<String, String> nodeunitTaskConfig) throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        // install required modules using npm
        final Map<String, String> npmTaskConfig = ImmutableMap.of(NpmConfigurator.COMMAND, "install nodeunit");
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install", npmTaskConfig);

        // add Nodeunit task
        taskConfigurationPage.addNewTask(NodeunitTaskComponent.TASK_NAME, NodeunitTaskComponent.class, null, nodeunitTaskConfig);

        return plan;
    }
}