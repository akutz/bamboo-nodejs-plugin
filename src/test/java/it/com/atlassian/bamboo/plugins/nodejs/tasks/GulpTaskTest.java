package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.ArtifactConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.plugins.nodejs.tasks.gulp.GulpConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.TestJobDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.GulpTaskComponent;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.io.File;
import java.util.Map;

public class GulpTaskTest extends AbstractNodeTaskTest
{
    @Test
    public void testTask() throws Exception
    {
        final ImmutableMap<String, String> baseGulpTaskConfig = ImmutableMap.of();
        runTaskAndCheckArtifact(baseGulpTaskConfig);
    }

    @Test
    public void testTaskWithAlternativeGulpfile() throws Exception
    {
        final ImmutableMap<String, String> baseGulpTaskConfig = ImmutableMap.of(
                TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY, "src",
                GulpConfigurator.GULP_RUNTIME, ".." + File.separator + GulpConfigurator.GULP_DEFAULT_EXECUTABLE,
                GulpConfigurator.CONFIG_FILE, ".." + File.separator + "Gulpfile.js");
        runTaskAndCheckArtifact(baseGulpTaskConfig);
    }

    private void runTaskAndCheckArtifact(@NotNull Map<String, String> baseGulpTaskConfig) throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // install required modules using npm
        final Map<String, String> npmTaskConfig = ImmutableMap.of(NpmConfigurator.COMMAND, "install gulp gulp-less");
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install", npmTaskConfig);

        // add Gulp 'less' task
        final Map<String, String> gulpTaskConfig = ImmutableMap.<String, String>builder()
                .putAll(baseGulpTaskConfig)
                .put(GulpConfigurator.TASK, "less")
                .build();

        taskConfigurationPage.addNewTask(GulpTaskComponent.TASK_NAME, GulpTaskComponent.class, "gulp less", gulpTaskConfig);

        // add artifact definition
        // TODO: actually check the artifact presence after execution, definition will not cause the task to fail
        final ArtifactConfigurationPage artifactConfigurationPage = product.visit(ArtifactConfigurationPage.class, defaultJob.getKey());
        artifactConfigurationPage.createArtifactDefinition("CSS files", "build", "*.css");

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);
    }

}