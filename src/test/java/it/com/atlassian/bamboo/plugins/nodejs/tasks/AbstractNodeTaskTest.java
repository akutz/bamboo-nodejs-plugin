package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.BambooTestedProductFactory;
import com.atlassian.bamboo.pageobjects.pages.global.BambooDashboardPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.CreatePlanPage;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.testutils.AcceptanceTestHelper;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.TestBuildDetailsHelper;
import com.atlassian.bamboo.testutils.backdoor.model.Result;
import com.atlassian.bamboo.testutils.config.BambooEnvironmentData;
import com.atlassian.bamboo.testutils.junit.rule.BackdoorRule;
import com.atlassian.bamboo.testutils.vcs.git.GitRepositoryDescriptor;
import com.atlassian.bamboo.testutils.vcs.git.LocalGitSetupHelper;
import com.atlassian.bamboo.webdriver.TestInjectionRule;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassian.util.concurrent.LazyReference;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;

import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;


public abstract class AbstractNodeTaskTest
{
    protected static final BambooTestedProduct product = BambooTestedProductFactory.create();
    protected static final LazyReference<GitRepositoryDescriptor> repository = new LazyReference<GitRepositoryDescriptor>()
    {
        @Override
        protected GitRepositoryDescriptor create() throws Exception
        {
            return LocalGitSetupHelper.createRepositoryFromZip("test-repository.zip");
        }
    };

    @Rule public final TestInjectionRule injectionRule = new TestInjectionRule(product);
    @Rule public final BackdoorRule backdoor = new BackdoorRule(new WebDriverTestEnvironmentData());
    @Rule public final WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    private final BambooEnvironmentData environmentData = new WebDriverTestEnvironmentData();
    private final List<PlanKey> plansToDelete = Lists.newArrayList();
    private final Properties i18nProperties = AcceptanceTestHelper.loadProperties("nodejs.properties");

    @Before
    public void setUp() throws Exception
    {
        backdoor.serverCapabilities().detectServerCapabilities();
    }

    @After
    public void tearDown()
    {
        for (final PlanKey planKey : plansToDelete)
        {
            backdoor.plans().deletePlan(planKey);
        }
        plansToDelete.clear();
    }

    @NotNull
    protected TestBuildDetails createAndSetupPlan() throws Exception
    {
        TestBuildDetails plan = TestBuildDetailsHelper.makeUniquePlan();
        TestBuildDetailsHelper.setupGitPlan(plan, repository.get());
        plan.setManualBuild(true);

        product.gotoLoginPage().loginAsSysAdmin(BambooDashboardPage.class);
        product.visit(CreatePlanPage.class).createNewPlan(plan);
        plansToDelete.add(plan.getKey());

        return plan;
    }

    protected int getTotalNumberOfTestsForBuild(@NotNull PlanKey planKey, int buildNumber) throws Exception
    {
        final Result buildResult = backdoor.plans().getBuildResult(planKey, buildNumber);
        return buildResult.getSuccessfulTestCount() + buildResult.getFailedTestCount() + buildResult.getQuarantinedTestCount();
    }

    protected int getNumberOfSuccessfulTestsForBuild(@NotNull PlanKey planKey, int buildNumber) throws Exception
    {
        final Result buildResult = backdoor.plans().getBuildResult(planKey, buildNumber);
        return buildResult.getSuccessfulTestCount();
    }

    @Nullable
    protected String getText(@NotNull String key, Object... args)
    {
        final String property = i18nProperties.getProperty(key);
        if (property == null)
        {
            return null;
        }
        else
        {
            return MessageFormat.format(property, args);
        }
    }
}