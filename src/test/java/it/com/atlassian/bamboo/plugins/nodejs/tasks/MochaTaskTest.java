package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.plugins.nodejs.tasks.mocha.runner.MochaRunnerConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.MochaParserTaskComponent;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.MochaRunnerTaskComponent;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MochaTaskTest extends AbstractNodeTaskTest
{
    @Test
    public void testTaskWithParsingEnabled() throws Exception
    {
        final Map<String, String> mochaRunnerTaskConfig = ImmutableMap.of(
                MochaRunnerConfigurator.TEST_FILES, "test/mocha/",
                MochaRunnerConfigurator.PARSE_TEST_RESULTS, Boolean.TRUE.toString());

        final TestBuildDetails plan = createMochaPlan(mochaRunnerTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);

        assertThat("Should have found 1 successful test", getNumberOfSuccessfulTestsForBuild(plan.getKey(), 1), is(1));
    }

    @Test
    public void testTaskWithMochaParser() throws Exception
    {
        final Map<String, String> mochaRunnerTaskConfig = ImmutableMap.of(
                MochaRunnerConfigurator.TEST_FILES, "test/mocha/",
                MochaRunnerConfigurator.PARSE_TEST_RESULTS, Boolean.FALSE.toString());

        final TestBuildDetails plan = createMochaPlan(mochaRunnerTaskConfig);

        // assert that with parsing disabled we don't see any test results
        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);

        assertThat("Shouldn't have found any test results", getTotalNumberOfTestsForBuild(plan.getKey(), 1), is(0));

        // add Mocha parser task
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());
        final Map<String, String> mochaParserTaskConfig = ImmutableMap.of();
        taskConfigurationPage.addNewTask(MochaParserTaskComponent.TASK_NAME, MochaParserTaskComponent.class, null, mochaParserTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 2);

        assertThat("Should have found 1 successful test", getNumberOfSuccessfulTestsForBuild(plan.getKey(), 2), is(1));
    }

    private TestBuildDetails createMochaPlan(@NotNull Map<String, String> mochaRunnerTaskConfig) throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        // install required modules using npm
        final Map<String, String> npmTaskConfig = ImmutableMap.of(NpmConfigurator.COMMAND, "install mocha mocha-bamboo-reporter");
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install", npmTaskConfig);

        // add Mocha runner task
        taskConfigurationPage.addNewTask(MochaRunnerTaskComponent.TASK_NAME, MochaRunnerTaskComponent.class, null, mochaRunnerTaskConfig);

        return plan;
    }
}