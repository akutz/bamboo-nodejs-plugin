package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.pageobjects.utils.FormUtils;
import com.atlassian.bamboo.plugins.nodejs.tasks.mocha.runner.MochaRunnerConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;
import org.apache.log4j.Logger;

import java.util.Map;

public class MochaRunnerTaskComponent extends ComponentWithAdvancedOptions implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(MochaRunnerTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Mocha Test Runner";

    // ------------------------------------------------------------------------------------------------- Type Properties
    @ElementBy(name = MochaRunnerConfigurator.NODE_RUNTIME)
    private SelectElement nodeRuntimeField;

    @ElementBy(name = MochaRunnerConfigurator.MOCHA_RUNTIME)
    private TextElement mochaRuntimeField;

    @ElementBy(name = MochaRunnerConfigurator.TEST_FILES)
    private TextElement testFilesField;

    @ElementBy(name = MochaRunnerConfigurator.PARSE_TEST_RESULTS)
    private CheckboxElement parseTestResultsCheckbox;

    @ElementBy(name = TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        this.withAdvancedOptions();

        if (config.containsKey(MochaRunnerConfigurator.NODE_RUNTIME))
        {
            nodeRuntimeField.select(Options.value(config.get(MochaRunnerConfigurator.NODE_RUNTIME)));
        }
        if (config.containsKey(MochaRunnerConfigurator.MOCHA_RUNTIME))
        {
            mochaRuntimeField.setText(config.get(MochaRunnerConfigurator.MOCHA_RUNTIME));
        }
        if (config.containsKey(MochaRunnerConfigurator.TEST_FILES))
        {
            testFilesField.setText(config.get(MochaRunnerConfigurator.TEST_FILES));
        }
        if (config.containsKey(MochaRunnerConfigurator.PARSE_TEST_RESULTS))
        {
            FormUtils.checkCheckbox(parseTestResultsCheckbox, Boolean.parseBoolean(config.get(MochaRunnerConfigurator.PARSE_TEST_RESULTS)));
        }
        if (config.containsKey(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES))
        {
            environmentVariablesField.setText(config.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES));
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}